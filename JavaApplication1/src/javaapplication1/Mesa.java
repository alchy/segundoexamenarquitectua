/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Bayern
 */
public class Mesa {
    private int tam;
    private int piezas[][];
    private int x_spc;
    private int y_spc;
    
    
    public Mesa(int tam) {
        this.tam=tam;
        piezas=new int[tam][tam];
    }
    
    public void iniciar(){
        int limite=tam*tam-1;
        for (int i =0; i <tam; i++) {
            for (int j = 0; j < tam; j++) {
               piezas[i][j]=limite;
               limite--;
            }
        }
        
        if (tam*tam%2==0){
            int aux=piezas[tam-1][tam-2];
            piezas[tam-1][tam-2]=piezas[tam-1][tam-3];
            piezas[tam-1][tam-3]=aux;
        }
        y_spc=x_spc=tam-1;
        
    }

    @Override
    public String toString() {
        String str="";
        for (int i =0; i <tam; i++) {
            for (int j = 0; j < tam; j++) {
               
               int pieza=piezas[i][j];
               if (Math.log10(pieza)+1>=2){
                  str+=""+piezas[i][j];
                  str+=" "   ;
               }
               else{
                 str+=" "+piezas[i][j];
                 str+=" ";
               }
            }
            str+="\n";
        }
        
        return str;
    }
    
    
    
    public boolean mover1(int pieza){
      if (x_spc==tam-1 && y_spc==tam-1){
        if (piezas[tam-2][tam-1]==pieza){
           piezas[tam-1][tam-1]=piezas[tam-2][tam-1];
           piezas[tam-2][tam-1]=0; 
           x_spc=tam-2;
           y_spc=tam-1;
           return true;
        }
        else if(piezas[tam-1][tam-2]==pieza){
           piezas[tam-1][tam-1]=piezas[tam-1][tam-2];
           piezas[tam-1][tam-2]=0; 
           x_spc=tam-1;
           y_spc=tam-2;
           return true;            
        }
    }
    else if (x_spc==tam-1 && y_spc>0 && y_spc<tam-1){
        if (piezas[x_spc][y_spc+1]==pieza){
          
           piezas[x_spc][y_spc]=piezas[x_spc][y_spc+1];
           piezas[x_spc][y_spc+1]=0;

           y_spc=y_spc+1;
           return true;
        }
        else if(piezas[x_spc][y_spc-1]==pieza){
            
           piezas[x_spc][y_spc]=piezas[x_spc][y_spc-1];
           piezas[x_spc][y_spc-1]=0;
           y_spc=y_spc-1;            
           return true;
        }
        else if (piezas[x_spc-1][y_spc]==pieza){

           piezas[x_spc][y_spc]=piezas[x_spc-1][y_spc];
           piezas[x_spc-1][y_spc]=0;
           x_spc=x_spc-1;
           return true;
        }
        
    }
    else if (x_spc==tam-1 && y_spc==0){
        if (piezas[tam-1][y_spc+1]==pieza){
            piezas[x_spc][y_spc]=piezas[tam-1][y_spc+1];
            piezas[tam-1][y_spc+1]=0;
            x_spc=tam-1;
            y_spc=y_spc+1;
            return true;
            
        }
        else if (piezas[x_spc-1][y_spc]==pieza){
            piezas[x_spc][y_spc]=piezas[x_spc-1][y_spc];
            piezas[x_spc-1][y_spc]=0;
            x_spc=x_spc-1;
            return true;
        }
    }
    else if (x_spc>0 && x_spc<tam-1 && y_spc==0){
        if (piezas[x_spc+1][y_spc]==pieza){
            piezas[x_spc][y_spc]=piezas[x_spc+1][y_spc];
            piezas[x_spc+1][y_spc]=0;
            x_spc=x_spc+1;
            return true;
        }
        else if(piezas[x_spc-1][y_spc]==pieza){
            piezas[x_spc][y_spc]=piezas[x_spc-1][y_spc];
            piezas[x_spc-1][y_spc]=0;
            x_spc=x_spc-1;
            return true;
            
        }
        else if (piezas[x_spc][y_spc+1]==pieza){
            piezas[x_spc][y_spc]=piezas[x_spc][y_spc+1];
            piezas[x_spc][y_spc+1]=0;
            y_spc=y_spc+1;
            return true;
        }
    
    }
    else if (x_spc==0 && y_spc==0){
       if (piezas[x_spc][y_spc+1]==pieza){
         piezas[x_spc][y_spc]=piezas[x_spc][y_spc+1];
         piezas[x_spc][y_spc+1]=0;
         y_spc=y_spc+1;
         return true;
       
       }
       else if(piezas[x_spc+1][y_spc]==pieza){
         piezas[x_spc][y_spc]=piezas[x_spc+1][y_spc];
         piezas[x_spc+1][y_spc]=0;
         x_spc=x_spc+1;
         return true;
       }
       
    }
    else if (x_spc==0 && y_spc>0 && y_spc<tam-1){
        if (piezas[x_spc][y_spc-1]==pieza){

            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc-1]=0;
            y_spc=y_spc-1;
            return true;

        }
        else if(piezas[x_spc][y_spc+1]==pieza){

            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc+1]=0;
            y_spc=y_spc+1;
            return true;
        }
        else if(piezas[x_spc+1][y_spc]==pieza){
            
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc+1][y_spc]=0;
            x_spc++;
            return true;
        }
    
    }
    else if(x_spc==0 && y_spc==tam-1){
        if (piezas[x_spc][y_spc-1]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc-1]=0;
            y_spc--;
            return true;
        }
        else if(piezas[x_spc+1][y_spc]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc+1][y_spc]=0;
            x_spc++;
            return true;
        
        }
    
    }
    else if (y_spc==tam-1 && x_spc>0 && x_spc<tam-1){

        if (piezas[x_spc][y_spc-1]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc-1]=0;
            y_spc--;
            return true;
    
        }
        else if(piezas[x_spc+1][y_spc]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc+1][y_spc]=0;
            x_spc++;
            return true;
            
        }
        else if(piezas[x_spc-1][y_spc]==pieza){
            piezas[x_spc][y_spc]=pieza;       
            piezas[x_spc-1][y_spc]=0;
            x_spc--;
            return true;
            

        }
    }
    else{
        if (piezas[x_spc][y_spc+1]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc+1]=0;
            y_spc++;
            return true;
            
            
        }
        else if(piezas[x_spc][y_spc-1]==pieza){
            piezas[x_spc][y_spc]=pieza;
            piezas[x_spc][y_spc-1]=0;
            y_spc--;
            return true;
            
            
        }
        else if(piezas[x_spc+1][y_spc]==pieza){
           piezas[x_spc][y_spc]=pieza;
           piezas[x_spc+1][y_spc]=0;
           x_spc++;
           return true;
           
        }
        else if (piezas[x_spc-1][y_spc]==pieza){
           piezas[x_spc][y_spc]=pieza;
           piezas[x_spc-1][y_spc]=0;
           x_spc--;
           return true;
           
        }
    }
    return false;
    }
    
    public boolean juegoGanado(){
        
        int aux=1;
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
               
                    if (piezas[i][j]!=aux){
                        return false;
                    }
                       if (aux<tam*tam-1){
                            aux++;
                        }
                        else{
                            aux=0;

                        }
            }
         
            
        }
        
        return true;
    }
    
    
    
    
    
}
