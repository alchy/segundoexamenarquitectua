/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Bayern
 */
public class MesaTest {
    public static void main(String[] args) throws IOException {
        Mesa m=new Mesa(4);
        m.iniciar();
        
        while(!m.juegoGanado()){
            System.out.println(m);
            
            System.out.println("Ingrese un numero");
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in)); 
            String number = bufferedreader.readLine(); 
           
            try{
                int value = Integer.parseInt(number);
                m.mover1(value);
            }
            catch(Exception e){
                System.out.println(e);
            }
            
            //Runtime.get1
            //Runtime().exec("cls");
            
        }
    }
}
