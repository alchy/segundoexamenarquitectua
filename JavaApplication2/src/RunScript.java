

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This code runs the system script
 * 
 * @author itcuties
 *
 */
public class RunScript {

	public static void main(String[] args) {
		try {
			// Run the process
			Process p = Runtime.getRuntime().exec("cmd /c  c:\\scripts\\test.bat");
			imprimier(p);
//                        Process p1 = Runtime.getRuntime().exec("cmd c:\\scripts\\test1.bat");
//                        imprimier(p1);
//                        // Get the input stream
//			 Process p2 = Runtime.getRuntime().exec("cmd c:\\scripts\\test2.bat");
//                         imprimier(p2);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
        public static void imprimier(Process p){
            try {
                InputStream is = p.getInputStream();
                                
                                // Read script execution results
                                int i = 0;
                                StringBuffer sb = new StringBuffer();
                                while ( (i = is.read()) != -1){
                                        sb.append((char)i);
                                        //System.out.println("hola mundo");
                                }
                                System.out.println(sb.toString());
            } catch (IOException ex) {
                Logger.getLogger(RunScript.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
