package com.generador.Presentacion.GenerarAplicacion;

import static com.generador.Presentacion.GenerarAplicacion.IUGenerarAplicacion.graph;
import com.generador.negocio.modeloConpetual.Atributo;
import com.generador.negocio.modeloConpetual.Tabla;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxRectangle;


public class AdicionarGrafo extends IUGenerarAplicacion {	
	
	public AdicionarGrafo(String nome){
		this.getGraph().getModel().beginUpdate();
		Object parent = this.getGraph().getDefaultParent(); 
		//Object v1 = this.getGraph().insertVertex(parent, null, nome, 330, 30, 100, 50);//
               Tabla tabla=new Tabla("alumno");
               tabla.agregarAtributo(new Atributo("nombre","string"));
               tabla.agregarAtributo(new Atributo("apellido","string"));
               tabla.agregarAtributo(new Atributo("registro","string"));
               tabla.agregarAtributo(new Atributo("contacto","string"));
               mxCell v1 = (mxCell) graph.insertVertex(parent, null,tabla,
			50, 50, 100, 20+20*3);
		v1.getGeometry().setAlternateBounds(new mxRectangle(0, 0, 140, 25));
		this.getM().put(nome, v1);
		this.getGraph().getModel().endUpdate();
	}

    AdicionarGrafo(Tabla tabla) {
        	this.getGraph().getModel().beginUpdate();
		Object parent = this.getGraph().getDefaultParent(); 
		//Object v1 = this.getGraph().insertVertex(parent, null, nome, 330, 30, 100, 50);//
           
               mxCell v1 = (mxCell) graph.insertVertex(parent, null,tabla,
			50, 50, 100, 20+20*3);
		v1.getGeometry().setAlternateBounds(new mxRectangle(0, 0, 140, 25));
		this.getM().put(tabla.getNombre(), v1);
		this.getGraph().getModel().endUpdate();
    }

}