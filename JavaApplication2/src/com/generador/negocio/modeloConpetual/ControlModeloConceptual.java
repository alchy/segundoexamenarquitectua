/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.modeloConpetual;

import java.util.ArrayList;

/**
 *
 * @author Bayern
 */
public class ControlModeloConceptual {
    private ArrayList<Tabla>tablas=new ArrayList<>();
    private String nombreAplicacion;
    
    
    public ControlModeloConceptual(String nombreApplicacion) {
        tablas=new ArrayList<>();
        this.nombreAplicacion=nombreApplicacion;
    }
    public void agregarTabla(Tabla tabla){
        tablas.add(tabla);
    }
    public void eliminarTabla(Tabla tabla){
        tablas.remove(tabla);
    }
    public void agregarRelacion(Tabla tabla1,Tabla tabla2){
        tabla1.agregarForeignKey(tabla2);
//Si no funciona buscar en la lista....                
    }
    
    public ArrayList<String>toScaffold(){
        ArrayList<String> scaffolds=new ArrayList<>();
        for (Tabla tabla: tablas) {
            scaffolds.add(tabla.toScaffold());
        }
        return scaffolds;
    }

    public ArrayList<Tabla> getTablas() {
        return tablas;
    }

    public void setTablas(ArrayList<Tabla> tablas) {
        this.tablas = tablas;
    }

    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

    public void setNombreAplicacion(String nombreAplicacion) {
        this.nombreAplicacion = nombreAplicacion;
    }
    
    
    
    
    
    
}
