/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.modeloConpetual;

import java.io.Serializable;

/**
 *
 * @author Bayern
 */
public class ForeingKey implements Serializable{
    private Tabla tabla;
    
    public ForeingKey() {
    }

    public ForeingKey(Tabla tabla) {
        this.tabla = tabla;
    }
    
    public String toScaffold(){
        return " "+tabla.getNombre()+":references";
    }
            
    
}
