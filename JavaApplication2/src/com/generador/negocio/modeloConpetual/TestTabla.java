/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.modeloConpetual;

import com.generador.negocio.GenerarAplicacion.ControlGenerarAplicacion;

/**
 *
 * @author Bayern
 */
public class TestTabla {
    public static void main(String[] args) {
        ControlModeloConceptual cmc=new ControlModeloConceptual("Colegio");
        Tabla Persona=new Tabla("persona");
        Persona.agregarAtributo(new Atributo("nombre","string"));
        Persona.agregarAtributo(new Atributo("apellidoMaterno","string"));        
        Persona.agregarAtributo(new Atributo("apellidoPaterno","string"));       
        Persona.agregarAtributo(new Atributo("registro","string"));
        cmc.agregarTabla(Persona);
        Tabla Telefono=new Tabla("telefono");
        Telefono.agregarAtributo(new Atributo("descripcion", "string"));
        cmc.agregarTabla(Telefono);
        Tabla Direccion=new Tabla("direccion");
        Direccion.agregarAtributo(new Atributo("descripcion", "string"));
        cmc.agregarTabla(Direccion);
        cmc.agregarRelacion(Persona,Telefono);
        cmc.agregarRelacion(Persona,Direccion);
        
        ControlGenerarAplicacion generarAplicacion=new ControlGenerarAplicacion(cmc);
        generarAplicacion.crearComandos();
        generarAplicacion.crearAplicacion();
        
    }
}
