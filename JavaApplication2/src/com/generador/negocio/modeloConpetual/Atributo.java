/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.modeloConpetual;

import java.io.Serializable;

/**
 *
 * @author Bayern
 */
public class Atributo implements Serializable{
    private String nombre;
    private String tipo;

    public Atributo(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public String toScaffold(){
        return nombre+":"+tipo;
    }

    @Override
    public String toString() {
        return nombre+" : "+tipo;
    }
    
}
