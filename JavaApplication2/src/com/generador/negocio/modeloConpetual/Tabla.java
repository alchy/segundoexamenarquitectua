/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.modeloConpetual;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Bayern
 */
public class Tabla implements Serializable{
    private ArrayList<Atributo>atributos;
    private ArrayList<ForeingKey>foreingKeys;
    
    private String nombre;

    public Tabla(String nombre) {
        atributos=new ArrayList();
        foreingKeys=new  ArrayList<>();
        this.nombre=nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void agregarAtributo(Atributo atributo){
        atributos.add(atributo);
    }
    public void agregarForeignKey(Tabla tabla){
       foreingKeys.add(new ForeingKey(tabla));
    }
    public String toScaffold(){
        String str=" ";
        for (Atributo atributo : atributos) {
            str+=" "+atributo.toScaffold();
        }
        for (ForeingKey foreingKey : foreingKeys) {
            str+=foreingKey.toScaffold();
        }
        return "rails generate scaffold "+nombre+str;
    }

    @Override
    public String toString() {
        String str=nombre+"\n";
        for (Atributo atributo : atributos) {
            str+="\n "+atributo.toString();
        }
        for (ForeingKey foreingKey : foreingKeys) {
            str+="\n"+foreingKey.toString();
        }
        
        return str;
    }
    
}
