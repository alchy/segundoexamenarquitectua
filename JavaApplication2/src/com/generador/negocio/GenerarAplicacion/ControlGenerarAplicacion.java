/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.negocio.GenerarAplicacion;


import com.generador.datos.Comando.DatosComando;
import com.generador.negocio.modeloConpetual.ControlModeloConceptual;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Bayern
 */
public class ControlGenerarAplicacion {
    private ControlModeloConceptual controlModeloConceptual;
    private DatosComando datosComando;
    private  String DIR_COMAND;
    private  String DIR_APP;
    private  String DIR_APP_ROOT;
    public ControlGenerarAplicacion(ControlModeloConceptual controlModeloConceptual) {
        this.controlModeloConceptual = controlModeloConceptual;
        
        DIR_COMAND="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator+"Comandos"+controlModeloConceptual.getNombreAplicacion()+File.separator;
        DIR_APP="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator+controlModeloConceptual.getNombreAplicacion()+File.separator;
        DIR_APP_ROOT="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator;
        datosComando=new DatosComando(DIR_COMAND,DIR_APP);
    }

    public String getDIR_COMAND() {
        return DIR_COMAND;
    }

    public String getDIR_APP() {
        return DIR_APP;
    }

    public String getDIR_APP_ROOT() {
        return DIR_APP_ROOT;
    }
    
    public String getAppName(){
        return controlModeloConceptual.getNombreAplicacion();
    }
    public void setAppName(String nombre){
       controlModeloConceptual.setNombreAplicacion(nombre);
       DIR_COMAND="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator+"Comandos"+controlModeloConceptual.getNombreAplicacion()+File.separator;
        DIR_APP="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator+controlModeloConceptual.getNombreAplicacion()+File.separator;
        DIR_APP_ROOT="f:"+File.separator+"Sites"+File.separator+"Generado"+controlModeloConceptual.getNombreAplicacion()+File.separator;
        datosComando=new DatosComando(DIR_COMAND,DIR_APP);
    }
    
    public void crearAplicacion(){
        crearComandos();
        datosComando.ejecutarComandos();
    }
    
    public void crearComandosPorDefecto(){
    
    }
    
    public void crearComandos(){
        ArrayList<String> comandos=controlModeloConceptual.toScaffold();
        int i=1;
        String comandoCrear="rails new "+controlModeloConceptual.getNombreAplicacion();
        String cdDirComand="cd "+this.DIR_APP_ROOT+" \n"+ comandoCrear;
        
        datosComando.guardarComando(cdDirComand, 0);
        for (String string : comandos) {
            String strCmd="cd "+DIR_APP+" \n"+string;
            datosComando.guardarComando(strCmd,i );
            i++;
        }
        
        String cmdMigrar="rake db:migrate";
        String strCmd="cd "+DIR_APP+" \n"+cmdMigrar;
        datosComando.guardarComando(strCmd, i);
    }
}
