/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.generador.datos.Comando;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bayern
 */
public class DatosComando {
    private final String DIR_APP;
    private final String DIR_COMMAND;

    public DatosComando(String DIR_COMAND, String DIR_APP) {
        this.DIR_COMMAND=DIR_COMAND;
        this.DIR_APP=DIR_APP;
    }
    
    public void guardarComando(String comando, int i){
        try {
            File file=new File(DIR_COMMAND+"comando"+i+".bat");
            if(!file.exists()){
                if (file.getParentFile().mkdirs()){
                    System.out.println("Se creo"+file.getAbsolutePath());
                }
                else{
                    System.out.println("file.getParentFile().mkdir() no funciono bien");
                }
            }
            else{
                System.out.println(" !file.exists() No se pudo Crear el archivo");
            }
            
               Formatter f=new Formatter(file);
                    f.format("%s",comando);
                    f.close();
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DatosComando.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void ejecutarComandos(){
        File  folder=new File(this.DIR_COMMAND);
        File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile()) {
                    ejecutarComando(file.getName());
                }
            }
    
    
    }
    
    public void ejecutarComando(String nombre){
        try {
            Process p = Runtime.getRuntime().exec( 
               new String[]{"cmd", "/C",nombre},
               null,
               new File(this.DIR_COMMAND));
            imprimirEjecucion(p);
        } catch (IOException ex) {
            Logger.getLogger(DatosComando.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void imprimirEjecucion(Process p){
               try {
                InputStream is = p.getInputStream();
                                
                                // Read script execution results
                                int i = 0;
                                StringBuffer sb = new StringBuffer();
                                while ( (i = is.read()) != -1){
                                        sb.append((char)i);
                                        //System.out.println("hola mundo");
                                }
                                System.out.println(sb.toString());
            } catch (IOException ex) {
                Logger.getLogger(DatosComando.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    
}
